/***************************************************************************
 *   Copyright (C) 2010 by Tn                                              *
 *   thenobody@poczta.fm                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef QLEDINDICATOR_H
#define QLEDINDICATOR_H

#include <QAbstractButton>
#include <QResizeEvent>
#include <QColor>

class QLedIndicator : public QAbstractButton {
    Q_PROPERTY(QColor onColor1      WRITE setOnColor1     READ onColor1   );
    Q_PROPERTY(QColor onColor2      WRITE setOnColor2     READ onColor2   );
    Q_PROPERTY(QColor offColor1     WRITE setOffColor1    READ offColor1  );
    Q_PROPERTY(QColor offColor2     WRITE setOffColor2    READ offColor2  );
    Q_OBJECT
public:
    explicit QLedIndicator(QWidget *parent = nullptr, int size = 24);

    const QColor &onColor1(void) const  { return m_onColor1;  }
    const QColor &offColor1(void) const { return m_offColor1; }
    const QColor &onColor2(void) const  { return m_onColor2;  }
    const QColor &offColor2(void) const { return m_offColor2; }

public Q_SLOTS:
    void setOnColor1(const QColor &c)  { m_onColor1  = c; }
    void setOffColor1(const QColor &c) { m_offColor1 = c; }
    void setOnColor2(const QColor &c)  { m_onColor2  = c; }
    void setOffColor2(const QColor &c) { m_offColor2 = c; }

    void setBlinkInterval(int ms) {m_blinkInterval = ms;}

    void blink();

private Q_SLOTS:
    void blinkOff();
protected:
    virtual void paintEvent (QPaintEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;
private:
    static constexpr qreal scaledSize {1000};
    QColor  m_onColor1, m_offColor1;
    QColor  m_onColor2, m_offColor2;
    QPixmap m_ledBuffer;
    int m_blinkInterval {200};
    bool m_blinkProgr {false};
    bool m_defBlink {false};

};

#endif // QLEDINDICATOR_H
