/***************************************************************************
 *   Copyright (C) 2010 by Tn                                              *
 *   thenobody@poczta.fm                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QPainter>
#include <QTimer>

#include "QLedIndicator.h"

QLedIndicator::QLedIndicator(QWidget *parent, int size) :
    QAbstractButton{parent},
    m_onColor1 {0, 255, 0},
    m_onColor2 {0, 192, 0},
    m_offColor1{0,  28, 0},
    m_offColor2{0, 128, 0} {

    setMinimumSize(size,size);
    setMaximumHeight(size);
    setCheckable(true);
}

void QLedIndicator::blink() {
    if (!m_blinkProgr) {
        m_blinkProgr = true;
        setChecked(true);
        QTimer::singleShot(m_blinkInterval, this, &QLedIndicator::blinkOff);
    } else {
        m_defBlink = true;
    }
}

void QLedIndicator::blinkOff() {
    if (isChecked()) {
        setChecked(false);
        QTimer::singleShot(m_blinkInterval, this, &QLedIndicator::blinkOff);
    } else {
        m_blinkProgr = false;
        if (m_defBlink) {
            m_defBlink = false;
            blink();
        }
    }
}

void QLedIndicator::resizeEvent(QResizeEvent *event) {
    update();
}

void QLedIndicator::paintEvent(QPaintEvent *event) {
    const qreal realSize {static_cast<qreal>(qMin(width(), height()))};

    QRadialGradient gradient;
    QPainter painter {this};
    QPen     pen {Qt::GlobalColor::black};

    pen.setWidth(1);

    painter.setRenderHint(QPainter::RenderHint::Antialiasing);
    painter.translate(width()/2, height()/2);
    painter.scale(realSize/scaledSize, realSize/scaledSize);

    gradient = QRadialGradient{QPointF{-500, -500}, 1500, QPointF{-500, -500}};
    gradient.setColorAt(0, QColor{224,224,224});
    gradient.setColorAt(1, QColor{ 28, 28, 28});
    painter.setPen(pen);
    painter.setBrush(QBrush{gradient});
    painter.drawEllipse(QPointF{0,0}, 500, 500);

    gradient = QRadialGradient{QPointF{500, 500}, 1500, QPointF{500,500}};
    gradient.setColorAt(0, QColor{224,224,224});
    gradient.setColorAt(1, QColor{ 28, 28, 28});
    painter.setPen(pen);
    painter.setBrush(QBrush{gradient});
    painter.drawEllipse(QPointF{0, 0}, 450, 450);

    painter.setPen(pen);
    if (isChecked()) {
        gradient = QRadialGradient{QPointF{-500, -500}, 1500, QPointF{-500, -500}};
        gradient.setColorAt(0, m_onColor1);
        gradient.setColorAt(1, m_onColor2);
    } else {
        gradient = QRadialGradient{QPointF{500, 500}, 1500, QPointF{500, 500}};
        gradient.setColorAt(0, m_offColor1);
        gradient.setColorAt(1, m_offColor2);
    }
    painter.setBrush(gradient);
    painter.drawEllipse(QPointF{0,0}, 400, 400);
}

